//
//  GCDConnection.swift
//  HTTPServer
//
//  Created by Nikolay Petrov on 6/30/16.
//  Copyright © 2016 Swift. All rights reserved.
//

#if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
    import Darwin
#elseif os(Linux)
    import Glibc
#endif

import Foundation
import Dispatch

let AllocCapacity = 8 * 1024

public class GCDConnection {
    private(set) weak var server: GCDServer?

    public let uuid = UUID()
    let socket: Int32

    public var receivedCallback: ((Data) -> Void)?
    public var disconnectedCallback: ((Void) -> Void)?

    let readQueue: DispatchQueue
    let readSource: DispatchSourceRead
    fileprivate let readBuffer = UnsafeMutablePointer<UInt8>.allocate(capacity: AllocCapacity)

    let writeQueue: DispatchQueue
    let writeSource: DispatchSourceWrite
    fileprivate var writeEmptyBuffer = Data()
    fileprivate var writeBuffers: [Data] = []

    init(server: GCDServer, socket: Int32) {
        self.server = server

        self.socket = socket

        self.readQueue = DispatchQueue(label: "gcdconnection.\(uuid).read")
        self.readSource = DispatchSource.makeReadSource(fileDescriptor: socket, queue: readQueue)

        self.writeQueue = DispatchQueue(label: "gcdconnection.\(uuid).write")
        self.writeSource = DispatchSource.makeWriteSource(fileDescriptor: socket, queue: writeQueue)

        readSource.setEventHandler { [weak self] in self?.readEvent() }
        readSource.setCancelHandler { [weak self] in self?.closeReadEvent() }

        writeSource.setEventHandler { [weak self] in self?.writeEvent() }
        writeSource.setCancelHandler { [weak self] in self?.closeWriteEvent() }
    }

    deinit {
        readBuffer.deallocate(capacity: AllocCapacity)
    }

    public func send(data: Data) {
        writeQueue.async { [weak self] in
            self?.writeBuffers.append(data)
            if !(self?.writeSource.isCancelled ?? true) {
                #if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
                    self?.writeSource.resume()
                #elseif os(Linux)
                    // Temporary solution
                    self?.writeEvent()
                #endif
            }
        }
    }

    public func disconnect() {
        readQueue.async { [weak self] in
            self?.disconnectDirect()
        }
    }
}

internal extension GCDConnection {
    func readEvent() {
        readBuffer.initialize(to: 0, count: AllocCapacity)
        defer {
            readBuffer.deinitialize(count: AllocCapacity)
        }

        let readSize = read(socket, UnsafeMutableRawPointer(readBuffer), AllocCapacity)
        if readSize < 0 {
            if readSize == Int(EAGAIN) {
                return print("read EAGAIN")
            }

            disconnectDirect()
            return print("read error")
        }
        if readSize == 0 {
            disconnectDirect()
            return
        }

        let data = Data(bytes: readBuffer, count: readSize)
        receivedCallback?(data)
    }

    func writeEvent() {
        guard let buffer = writeBuffers.first else {
            return writeSource.suspend()
        }

        let dataWrote = buffer.withUnsafeBytes { (bytes: UnsafePointer<UInt8>) -> Int? in
            let writeSize = write(self.socket, bytes, buffer.count)
            if writeSize < 0 {
                if writeSize == Int(EWOULDBLOCK) {
                    print("write EWOULDBLOCK")
                } else {
                    print("write error")
                }
                return nil
            }

            return writeSize
        }

        if let actualCount = dataWrote {
            if actualCount == buffer.count {
                writeBuffers.removeFirst()
            } else {
                var newBuffer = buffer

                let endReplacement = buffer.index(0, offsetBy: actualCount)
                newBuffer.replaceSubrange(0..<endReplacement, with: writeEmptyBuffer)

                self.writeBuffers[0] = newBuffer
            }
        }

        if writeBuffers.isEmpty {
            writeSource.suspend()
        }
    }

    func disconnectDirect() {
        writeQueue.sync {
            if writeBuffers.isEmpty {
                writeSource.resume()
            }
            writeSource.cancel()
        }
        readSource.cancel()
    }
    
    func closeReadEvent() {
        close(socket)
        self.disconnectedCallback?()
        
        server?.disconnectEvent(connection: self)
    }

    func closeWriteEvent() {
    }
}

